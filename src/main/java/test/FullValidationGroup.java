package test;

import javax.validation.GroupSequence;
import javax.validation.groups.Default;

@GroupSequence({Default.class, OperatorFormDataValidationGroup.class})
public interface FullValidationGroup {
    
}
