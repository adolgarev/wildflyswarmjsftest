package test;

import java.io.Serializable;
import javax.validation.constraints.NotNull;

public class OperatorFormData implements Serializable {

    @NotNull(groups=OperatorFormDataValidationGroup.class)
    private String name;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
