package test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Named
@SessionScoped
public class Match implements Serializable {

    @Min(1)
    @Max(5)
    private Integer numberOfPlayers = 1;

    @NotNull
    private SituationType selectedSituation;
    
    private final Integer[] PossibleNumberOfPlayers = {1, 2, 3, 4, 5};
    
    @Valid
    private final List<OperatorFormData> operatorsFormData = new ArrayList<>(PossibleNumberOfPlayers.length);

    public Integer getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(Integer numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public SituationType getSelectedSituation() {
        return selectedSituation;
    }

    public void setSelectedSituation(SituationType selectedSituation) {
        this.selectedSituation = selectedSituation;
    }
    
    public Integer[] getPossibleNumberOfPlayers() {
        return PossibleNumberOfPlayers;
    }

    public Integer[] getPossibleNumberOfPlayers2() {
        return PossibleNumberOfPlayers;
    }

    public List<OperatorFormData> getOperatorsFormData() {
        while (operatorsFormData.size() < getNumberOfPlayers())
            operatorsFormData.add(new OperatorFormData());
        return operatorsFormData;
    }
    
    public SituationType[] getAvailableSituations() {
        return SituationType.values();
    }
    
    public String start() {
        return "match?faces-redirect=true;";
    }
    
    public String restart() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index?faces-redirect=true";
    }
}
