package test;

public enum SituationType {
    Situation1("Situation 1");

    private final String name;

    SituationType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
